import React , { Component } from "react"
import Head from "./app"
import HeadTitle from "./HeadTitle"
import HeadFlag from "./HeadFlag"
class Example extends Component {
    constructor (props) {
        super(props)
        const self = this
        self.state = {

        }
    }
    render() {
        const self = this
        return (
            <div>
                <Head title="登录" 
                    onBack={function(){
                        console.log('go to')
                    }}
                />
                <HeadTitle>我的</HeadTitle>
                <HeadFlag>产品介绍</HeadFlag>
            </div>
        )
    }
}
Example = require('react-hot-loader').hot(module)(Example)

import ReactDOM from "react-dom"
ReactDOM.render(
    <Example />,
    document.getElementById('app')
)
