import React , { Component } from "react"
import NavFoot from "./app"

class Example extends Component {
    constructor (props) {
        super(props)
        const self = this
        self.state = {

        }
    }
    render() {
        const self = this
        return (
            <div>
                <NavFoot 
                    on="/"
                    data={[
                        {
                            title:'首页',
                            icon:'home',
                            link:'/home',
                        },{
                            title:'全部产品',
                            icon:'classify',
                            link:'/list',
                        },{
                            title:'我的',
                            icon:'people',
                            link:'/user',
                        }
                    ]}
                />
            </div>
        )
    }
}
Example = require('react-hot-loader').hot(module)(Example)

import ReactDOM from "react-dom"
ReactDOM.render(
    <Example />,
    document.getElementById('foot_demo')
)
