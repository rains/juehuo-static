import React , { Component } from "react"
import NavCrumbs from "./app"

class Example extends Component {
    render() {
        const self = this
        return (
            <div>
                <NavCrumbs 
                    data={[
                        {
                            title:'推荐产品',
                            icon:'support',
                            link:'/list?support',
                        },{
                            title:'财产险',
                            icon:'treasure',
                            link:'/list?treasure',
                        },{
                            title:'意外险',
                            icon:'accident',
                            link:'/list?accident',
                        },,{
                            title:'健康险',
                            icon:'health',
                            link:'/list?health',
                        },,{
                            title:'旅游险',
                            icon:'travel',
                            link:'/list?travel',
                        },
                    ]}
                />
            </div>
        )
    }
}
Example = require('react-hot-loader').hot(module)(Example)

import ReactDOM from "react-dom"
ReactDOM.render(
    <Example />,
    document.getElementById('crumbs_demo')
)
