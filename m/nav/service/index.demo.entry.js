import React , { Component } from "react"
import NavService from "./app"

class Example extends Component {
    constructor (props) {
        super(props)
        const self = this
        self.state = {

        }
    }
    render() {
        const self = this
        return (
            <div>
                <NavService 
                    price={'198.0'}
                    onClick={function(){
                        console.log('to pay')
                    }}
                />
            </div>
        )
    }
}
Example = require('react-hot-loader').hot(module)(Example)

import ReactDOM from "react-dom"
ReactDOM.render(
    <Example />,
    document.getElementById('service_demo')
)
