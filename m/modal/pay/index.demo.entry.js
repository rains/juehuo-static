import React , { Component } from "react"
import ModalPay from "./app"

class Example extends Component {
    constructor (props) {
        super(props)
        const self = this
        self.state = {
            show:true
        }
    }
    render() {
        const self = this
        return (
            <div>
                <button
                    onClick={function(){
                        self.setState({
                            show:true
                        })
                    }}
                >click to show</button>
                <ModalPay 
                    show={self.state.show}
                    onClose={function(){
                        self.setState({
                            show:false
                        })
                    }}
                    onSelect={function(type){
                        // type: wechat | alipay
                        console.log(type)
                    }}
                />
            </div>
        )
    }
}
Example = require('react-hot-loader').hot(module)(Example)

import ReactDOM from "react-dom"
ReactDOM.render(
    <Example />,
    document.getElementById('pay_demo')
)
