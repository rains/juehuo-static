import Icon from "./index"
import { Component } from "react"
import { render } from 'react-dom'

class Demo extends Component {
    constructor(props) {
        super(props)
        this.state = {
            one: false
        }
    }
    render() {
        const self = this
        return (
            <div style={{"fontSize":"30px"}}>
    			<Icon type="user" />
                <Icon type="$tech" />
            </div>
        )
    }
}

export default Demo

render(<Demo />,document.getElementById('iconDemo'))