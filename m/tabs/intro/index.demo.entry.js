import React, {Component} from 'react';
import ReactDom from 'react-dom';
import TabsIntro from './app'

class App extends Component {
	constructor(props) {
		super(props);
		this.state = {
			value:'1'
		}
	}

	render() {
		let self = this
		return (
			<TabsIntro
				value={self.state.value}
				data={[
					{
						infoId:'1',
						planPrice:198,
						planLimit:"58.7万保额",
						attrValModels:[
							{ name:'意外伤害', value:'10万元' },
							{ name:'意外医疗', value:'10万元' },
							{ name:'公共交通意外', value:'10万元' },
						],
						superiImgs:[
							'https://unsplash.it/200/300/?random',
							'https://unsplash.it/200/300/?random'
						]
					},{
						infoId:'2',
						planPrice:198,
						planLimit:"58.7万保额",
					},{
						infoId:'3',
						planPrice:198,
						planLimit:"58.7万保额",
					}
				]}
				onSwitch={function(value){
					console.log(value)
				}}
			/>
		);
	}
}
ReactDom.render(
	<App />,
	document.getElementById('intro_demo')
)