import React , { Component } from "react"
import Icon from "../../icons/index"
import BoxPlane from "./app"

class Example extends Component {
    constructor (props) {
        super(props)
        const self = this
        self.state = {

        }
    }
    render() {
        const self = this
        return (
            <div>
                <BoxPlane 
                    icon={(<Icon type="$home2"/>)}
                    title="登录"
                >
                    cnt
                </BoxPlane>

                <BoxPlane 
                    icon={(<Icon type="$home2"/>)}
                    title="登录"
                    tool={(
                        <div>
                            <Icon type="$ban" 
                                onClick={function(){
                                    console.log('click')
                                }}
                            />
                        </div>
                    )}
                >
                    cnt
                </BoxPlane>
            </div>
        )
    }
}
Example = require('react-hot-loader').hot(module)(Example)

import ReactDOM from "react-dom"
ReactDOM.render(
    <Example />,
    document.getElementById('plane_demo')
)
