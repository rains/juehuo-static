import React, {Component} from 'react';
import ReactDom from 'react-dom'
import Login from './index'

class App extends Component {
	render() {
		return (
			<Login/>
		);
	}
}

ReactDom.render(
	<App/>,
	document.getElementById('demo')
)