import React , { Component } from "react"
import ListHoribox from "./app"

class Example extends Component {
    constructor (props) {
        super(props)
        const self = this
        self.state = {

        }
    }
    render() {
        const self = this
        return (
            <div>
                <ListHoribox 
                    data={[
                        {
                            title:`平安.交通意外险`,
                            tip:'最高100万',
                            pic:'https://unsplash.it/200/300/?random',
                            link: 'http://pingan.com',
                        },{
                            title:`每天福利.幸运大抽奖`,
                            tip:'海量好礼等你拿',
                            pic:'https://unsplash.it/200/300/?random',
                            link: 'http://pingan.com',
                        },{
                            title:`平安.交通意外险`,
                            tip:'最高100万',
                            pic:'https://unsplash.it/200/300/?random',
                            link: 'http://pingan.com',
                        },{
                            title:`每天福利.幸运大抽奖`,
                            tip:'海量好礼等你拿',
                            pic:'https://unsplash.it/200/300/?random',
                            link: 'http://pingan.com',
                        },{
                            title:`平安.交通意外险`,
                            tip:'最高100万',
                            pic:'https://unsplash.it/200/300/?random',
                            link: 'http://pingan.com',
                        },{
                            title:`每天福利.幸运大抽奖`,
                            tip:'海量好礼等你拿',
                            pic:'https://unsplash.it/200/300/?random',
                            link: 'http://pingan.com',
                        }
                    ]}
                />
            </div>
        )
    }
}
Example = require('react-hot-loader').hot(module)(Example)

import ReactDOM from "react-dom"
ReactDOM.render(
    <Example />,
    document.getElementById('horibox_demo')
)
