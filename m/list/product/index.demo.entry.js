import React , { Component } from "react"
import ListProduct from "./app"

class Example extends Component {
    constructor (props) {
        super(props)
        const self = this
        self.state = {

        }
    }
    render() {
        const self = this
        return (
            <div>
                <ListProduct 
                    data={[
                        {
                            planClassId:1,
                            title:`黄金貔貅手串  硬足金5.18克黄金18克黄金`,
                            bg:'https://unsplash.it/200/300/?random',
                            dec:"家庭成员责任险，为您全家撑起保护伞",
                            planPrice: 5,
                        },{
                            planClassId:2,
                            title:`黄金貔貅手串  硬足金5.18克黄金18克黄金`,
                            bg:'https://unsplash.it/200/300/?random',
                            dec:"家庭成员责任险，为您全家撑起保护伞",
                            planPrice: 5,
                        }
                    ]}
                />
            </div>
        )
    }
}
Example = require('react-hot-loader').hot(module)(Example)

import ReactDOM from "react-dom"
ReactDOM.render(
    <Example />,
    document.getElementById('product_demo')
)
