import React , { Component } from "react"
import ListText from "./app"
import ListTextInfo from "./Info"

class Example extends Component {
    constructor (props) {
        super(props)
        const self = this
        self.state = {

        }
    }
    render() {
        const self = this
        return (
            <div>
                <ListText 
                    data={[
                        {
                            date:'保障期限：2018-09-11至2019-09-22',
                            id:'1',
                            title:'title1',
                            _status:'overdue | active | unpaid | waitingEffect',
                            status:'overdue'
                        },{
                            date:'保障期限：2018-09-11至2019-09-22',
                            id:'2',
                            title:'title2',
                            _status:'overdue | active | unpaid | waitingEffect',
                            status:'unpaid'
                        },{
                            date:'保障期限：2018-09-11至2019-09-22',
                            id:'3',
                            title:'title3',
                            _status:'overdue | active | unpaid | waitingEffect',
                            status:'active'
                        },{
                            date:'保障期限：2018-09-11至2019-09-22',
                            id:'3',
                            title:'title3',
                            _status:'overdue | active | unpaid | waitingEffect',
                            status:'waitingEffect'
                        }
                    ]}
                />
                <hr/>

                <ListTextInfo 
                    data={[
                        {
                            planName:'title1',
                            id:'1',
                            _status:'overdue | active | unpaid | waitingEffect',
                            status:'overdue',
                            companyNames:['asdf']
                        },{
                            planName:'title2',
                            id:'1',
                            _status:'overdue | active | unpaid | waitingEffect',
                            status:'unpaid',
                            companyNames:['asdf']
                        },{
                            planName:'title3',
                            id:'1',
                            _status:'overdue | active | unpaid | waitingEffect',
                            status:'active',
                            companyNames:['asdf']
                        },{
                            planName:'title3',
                            id:'1',
                            _status:'overdue | active | unpaid | waitingEffect',
                            status:'waitingEffect',
                            companyNames:['asdf']
                        }
                    ]}
                />
                <hr/>
                <ListTextInfo 
                    data={[{
                        id:'1',
                            planName:'title1',
                            _status:'overdue | active | unpaid',
                            status:'active',
                            companyNames:['asdf']
                    }]}
                />
            </div>
        )
    }
}
Example = require('react-hot-loader').hot(module)(Example)

import ReactDOM from "react-dom"
ReactDOM.render(
    <Example />,
    document.getElementById('text_demo')
)
