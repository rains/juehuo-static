import React, {Component} from "react"
import ReactDOM from "react-dom"
import Icon from "../../icons/index"
import ListNav from "./app"

class App extends Component {
    constructor (props) {
        super(props)
        this.state = {

        }
    }
    render() {
        let self = this
        return (
            <div>
                <ListNav 
                    icon={(<Icon type="$home2"/>)}
                    title="房屋结构"
                    tip={false}
                />
                <ListNav 
                    icon={(<Icon type="$safelist"/>)}
                    title="我的保单"
                    tip={(<Icon type="$user"/>)}
                    link="/"
                />
                <ListNav 
                    icon={(<Icon type="$letter"/>)}
                    title="理赔申请"
                    link="/"
                />
                <ListNav 
                    icon={(<Icon type="$info"/>)}
                    title="关于我们"
                    link="/"
                />
            </div>
        )
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('nav_demo')
)
