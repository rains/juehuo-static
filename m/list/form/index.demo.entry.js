import React, {Component} from "react"
import ReactDOM from "react-dom"
import Icon from "../../icons/index"
import BoxPlane from "../../box/plane/app"
import BoxSim from "../../box/simple/app"
import ListForm from "./app"

class App extends Component {
    constructor (props) {
        super(props)
        this.state = {

        }
    }
    render() {
        let self = this
        return (
            <div>
                <BoxPlane 
                    icon={(<Icon type="$home2" />)}
                    title="家庭信息"
                >
                    <ListForm label="房屋结构">
                        <input type="text" placeholder="请填写" />
                    </ListForm>
                    <ListForm label="与投保人关系" arrow={true}>
                        <input type="text" placeholder="请填写" />
                    </ListForm>
                </BoxPlane>
                <hr/>
                <BoxSim>
                    <ListForm label="保障开始时间">
                        <div>2018-09-09</div>
                    </ListForm>
                    <ListForm label="保障结算时间" arrow={true}>
                        <div>2018-09-09</div>
                    </ListForm>
                    <ListForm 
                        label={(<div>查看<a href="">《保险条款》</a></div>)}
                    >
                    </ListForm>
                </BoxSim>
            </div>
        )
    }
}

ReactDOM.render(
    <App />,
    document.getElementById('form_demo')
)
