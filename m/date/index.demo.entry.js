import React , { Component } from "react"
import ReactDOM from "react-dom"
import DatePicker from 'react-mobile-datepicker';
import dayjs from "dayjs"
class Demo extends Component {
    constructor (props) {
        super(props)
        const self = this
        self.state = {
            time: new Date()
        }
    }
    handleSelect = (time) => {
        const self = this
        self.setState({
            time: time
        })
    }
    render() {
        const self = this
        return (
            <div>
                <DatePicker
                    value={this.state.time}
                    isOpen={true}
                    min={new Date(2018,9-1,7)}
                    max={new Date(2018,9-1,20)}
                    onSelect={this.handleSelect}
                    onCancel={this.handleCancel}
                />
                {dayjs(this.state.time).format('YYYY-MM-DD')}
            </div>
        )
    }
}

Demo = require('react-hot-loader').hot(module)(Demo)
ReactDOM.render(
    <Demo />,
    document.getElementById('app')
)
